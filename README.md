# autobots_ui_docker_pull_instruction

BLUE_THEME
1. go to https://hub.docker.com/r/thiethaa/alc-autobots-ui/tags?page=1&ordering=last_updated
2. open terminal and run this commands:

    docker pull thiethaa/alc-autobots-ui:latest

3. check if the image successfully pulled from docker hub : docker image_name: thiethaa/alc-autobots-ui

    docker image ls

4. run and build docker image 

    docker run -p 4040:3000 thiethaa/alc-autobots-ui

5. go to browser, the App should be run on port http://localhost:4040/

RED_THEME
1. go to https://hub.docker.com/r/thiethaa/alc-autobots-ui-red-theme/tags?page=1&ordering=last_updated
2. open terminal and run this commands:

    docker pull thiethaa/alc-autobots-ui-red-theme:latest

3. check if the image successfully pulled from docker hub : docker image_name: thiethaa/alc-autobots-ui-red-theme

    docker image ls

4. run and build docker image 

    docker run -p 6060:3000 thiethaa/alc-autobots-ui-red-theme

5. go to browser, the App should be run on port http://localhost:6060/
